
cd C:\Users\FCBR SIG\threadfix

del target\*.*

cov-build --dir intDir/ --fs-capture-search . mvn package

cov-analyze --dir intDir/ --all --trial --concurrency --disable-fb --preview --rule --security  --webapp-security --webapp-security-preview --webapp-security-trial 

cov-commit-defects --dir intDir/ --host localhost --user admin --password FCBr1000 --stream threadfix-demo
