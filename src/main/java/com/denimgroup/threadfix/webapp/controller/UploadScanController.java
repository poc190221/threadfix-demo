////////////////////////////////////////////////////////////////////////
//
//     Copyright (c) 2009-2011 Denim Group, Ltd.
//
//     The contents of this file are subject to the Mozilla Public License
//     Version 1.1 (the "License"); you may not use this file except in
//     compliance with the License. You may obtain a copy of the License at
//     http://www.mozilla.org/MPL/
//
//     Software distributed under the License is distributed on an "AS IS"
//     basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
//     License for the specific language governing rights and limitations
//     under the License.
//
//     The Original Code is Vulnerability Manager.
//
//     The Initial Developer of the Original Code is Denim Group, Ltd.
//     Portions created by Denim Group, Ltd. are Copyright (C)
//     Denim Group, Ltd. All Rights Reserved.
//
//     Contributor(s): Denim Group, Ltd.
//
////////////////////////////////////////////////////////////////////////
package com.denimgroup.threadfix.webapp.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.denimgroup.threadfix.data.entities.Application;
import com.denimgroup.threadfix.service.ApplicationService;
import com.denimgroup.threadfix.service.ScanService;
import com.denimgroup.threadfix.service.channel.ChannelImporter;

@Controller
@RequestMapping("/organizations/{orgId}/applications/{appId}/scans/upload")
public class UploadScanController {

	private ScanService scanService;
	private ApplicationService applicationService;
	
	private final Log log = LogFactory.getLog(UploadScanController.class);

	@Autowired
	public UploadScanController(ScanService scanService,
			ApplicationService applicationService) {
		this.scanService = scanService;
		this.applicationService = applicationService;
	}

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView uploadIndex(@PathVariable("orgId") int orgId,
			@PathVariable("appId") int appId) {
		return index(orgId, appId, null);
	}
	
	private ModelAndView index(int orgId, int appId, String message) {
		Application application = applicationService.loadApplication(appId);
		
		if (application == null) {
			log.warn(ResourceNotFoundException.getLogMessage("Application", appId));
			throw new ResourceNotFoundException();
		}
		
		if (application.getUploadableChannels() == null || application.getUploadableChannels().size() == 0) {
			log.info("The Application didn't have any channels, redirecting to the Add Channel page.");
			return new ModelAndView("redirect:/organizations/" + orgId + "/applications/" + appId + "/addChannel");
		}
		
		ModelAndView mav = new ModelAndView("scans/upload");
		mav.addObject(application);
		mav.addObject("message",message);
		return mav;
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView uploadSubmit(@PathVariable("appId") int appId, HttpServletRequest request,
			@RequestParam("channelId") Integer channelId, @RequestParam("file") MultipartFile file) {
		
		String returnValue = scanService.checkFile(channelId, file); 
		
		Application app = applicationService.loadApplication(appId);
		
		if (app == null || !app.isActive()) {
			log.warn(ResourceNotFoundException.getLogMessage("Application",appId));
			throw new ResourceNotFoundException();
		}
		
		if (ChannelImporter.SUCCESSFUL_SCAN.equals(returnValue)) {
			scanService.saveFileAndAddToQueue(Integer.valueOf(channelId), file);
		} else if (ChannelImporter.EMPTY_SCAN_ERROR.equals(returnValue)) {
			Integer emptyScanId = scanService.saveEmptyScanAndGetId(channelId, file);
			ModelAndView confirmPage = new ModelAndView("scans/confirm");
			confirmPage.addObject("scanId", emptyScanId);
			return confirmPage;
		} else {
			if (app.getId() != null && app.getOrganization() != null && app.getOrganization().getId() != null) {
				return index(app.getOrganization().getId(), app.getId(), returnValue);
			} else {
				log.warn("The request included an invalidly configured Application, throwing ResourceNotFoundException.");
				throw new ResourceNotFoundException();
			}
		}

		if (app.getOrganization() != null) {
			request.getSession().setAttribute("scanSuccessMessage", "The scan was successfully added to the queue for processing.");
			return new ModelAndView("redirect:/organizations/" + app.getOrganization().getId() + 
					"/applications/" + app.getId());
		} else {
			log.warn("Redirecting to the jobs page because it was impossible to redirect to the Application.");
			return new ModelAndView("redirect:/jobs/open");
		}
	}
	
	@RequestMapping(value = "/{emptyScanId}/confirm", method = RequestMethod.GET)
	public ModelAndView confirm(@PathVariable("orgId") Integer orgId,
			@PathVariable("appId") Integer appId, 
			@PathVariable("emptyScanId") Integer emptyScanId,
			HttpServletRequest request) {
		
		scanService.addEmptyScanToQueue(emptyScanId);
		
		Application app = applicationService.loadApplication(appId);
		
		if (app == null || !app.isActive()) {
			log.warn(ResourceNotFoundException.getLogMessage("Application",appId));
			throw new ResourceNotFoundException();
		}

		if (app.getOrganization() != null && app.getOrganization().getId() != null) {
			request.getSession().setAttribute("scanSuccessMessage", "The empty scan was successfully added to the queue for processing.");
			return new ModelAndView("redirect:/organizations/" + app.getOrganization().getId() + 
					"/applications/" + app.getId());
		} else {
			return new ModelAndView("redirect:/jobs/open");
		}
	}
	
	@RequestMapping(value = "/{emptyScanId}/cancel", method = RequestMethod.GET)
	public String cancel(@PathVariable("orgId") Integer orgId,
			@PathVariable("appId") Integer appId, 
			@PathVariable("emptyScanId") Integer emptyScanId) {
		
		scanService.deleteEmptyScan(emptyScanId);		
		return "redirect:/organizations/" + orgId + "/applications/" + appId + "/scans/upload";
	}
}
