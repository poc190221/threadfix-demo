<%@ include file="/common/taglibs.jsp"%>

<div id="footer">
<div id="poweredBy">ThreadFix is Powered by:</div>
<div id="bottomLogo">
	<a href="http://www.denimgroup.com/" class="denim-group" target="_blank">
		<img src="<%=request.getContextPath()%>/images/dg_logo_white.png" class="transparent_png"
		alt="Denim Group" />
	</a>
</div>
<div id="copyright">
&copy; 2010 - 2012  Denim Group, Ltd. Some Rights Reserved.
</div>

</div>
