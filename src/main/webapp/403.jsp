<%@ include file="/common/taglibs.jsp"%>

<page:applyDecorator name="default">
<head>
    <title><spring:message code="403.title"/></title>
    <meta name="heading" content="<spring:message code="403.title"/>"/>
</head>
<h2>Error</h2>
<br/>
<p>
    <spring:message code="403.message"/>
</p>
</page:applyDecorator>