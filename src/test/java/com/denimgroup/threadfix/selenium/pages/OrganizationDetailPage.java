////////////////////////////////////////////////////////////////////////
//
//     Copyright (c) 2009-2011 Denim Group, Ltd.
//
//     The contents of this file are subject to the Mozilla Public License
//     Version 1.1 (the "License"); you may not use this file except in
//     compliance with the License. You may obtain a copy of the License at
//     http://www.mozilla.org/MPL/
//
//     Software distributed under the License is distributed on an "AS IS"
//     basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
//     License for the specific language governing rights and limitations
//     under the License.
//
//     The Original Code is Vulnerability Manager.
//
//     The Initial Developer of the Original Code is Denim Group, Ltd.
//     Portions created by Denim Group, Ltd. are Copyright (C)
//     Denim Group, Ltd. All Rights Reserved.
//
//     Contributor(s): Denim Group, Ltd.
//
////////////////////////////////////////////////////////////////////////
package com.denimgroup.threadfix.selenium.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class OrganizationDetailPage extends BasePage {
	
	private WebElement orgName;
	private WebElement backToList;
	private WebElement deleteButton;
	private WebElement editOrganizationLink;
	private WebElement applicationsTableBody;
	private WebElement lastItemFoundInApplicationsTableBodyLink;
	private WebElement addApplicationLink;
	
	public OrganizationDetailPage(WebDriver webdriver) {
		super(webdriver);
		
		orgName = driver.findElementById("name");
		backToList = driver.findElementById("backToList");
		deleteButton = driver.findElementById("deleteLink");
		editOrganizationLink = driver.findElementById("editOrganizationLink");
		applicationsTableBody = driver.findElementById("applicationsTableBody");
		addApplicationLink = driver.findElementById("addApplicationLink");
	}
	
	public String getOrgName() {
		return orgName.getText();
	}
	
	public OrganizationIndexPage clickBackToList() {
		backToList.click();
		return new OrganizationIndexPage(driver);
	}
	public OrganizationEditPage clickEditOrganizationLink() {
		editOrganizationLink.click();
		return new OrganizationEditPage(driver);
	}
	
	public OrganizationIndexPage clickDeleteButton() {
		deleteButton.click();
		
		Alert alert = driver.switchTo().alert();
		alert.accept();
		
		return new OrganizationIndexPage(driver);
	}
	
	public boolean isTextPresentInApplicationsTableBody(String text) {
		for (WebElement element : applicationsTableBody.findElements(By.xpath(".//tr/td/a"))) {
			if (element.getText().contains(text)) {
				lastItemFoundInApplicationsTableBodyLink = element;
				return true;
			}
		}
		return false;
	}

	public ApplicationDetailPage clickTextLinkInApplicationsTableBody(String text) {
		if (isTextPresentInApplicationsTableBody(text)) {
			lastItemFoundInApplicationsTableBodyLink.click();
			return new ApplicationDetailPage(driver);
		} else {
			return null;
		}
	}

	public ApplicationAddPage clickAddApplicationLink() {
		addApplicationLink.click();
		return new ApplicationAddPage(driver);
	}
}
